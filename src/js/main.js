let mobileBtn = document.querySelector('.main-navm__mobile-btn');
let menu = document.querySelector('.main-navm');
let subBtns = document.getElementsByClassName('mlist-servs__btn-more');
let quests = document.getElementsByClassName('question__btn');
let moreRev = document.querySelector('.open-revs');
let moreProjRev = document.querySelector('.open-projrevs');
let showForm = document.querySelector('.show-form');
mobileBtn.addEventListener('click', function(){
    menu.classList.toggle('main-navm--open');
    this.classList.toggle('mobile-btnm--open');
})
for (i = 0; i<subBtns.length; i++){
    subBtns[i].addEventListener('click', function(){
        this.parentElement.classList.toggle('open-sub');
        this.classList.toggle('mlist-servs__btn-more--open');
    })
}

let tabBtns = document.getElementsByClassName('mtabs__btn');
if (tabBtns){
    let tabs = document.getElementsByClassName('mtab'),
    targetTab;

    for (i = 0; i<tabBtns.length; i++){
        tabBtns[i].addEventListener('click', function(){
            for (i = 0; i<tabBtns.length; i++){
                tabBtns[i].classList.remove('mtabs__btn--active');
                this.classList.add('mtabs__btn--active');
            }
            for (i = 0; i<tabs.length; i++){
                tabs[i].classList.remove('mtabs__one--active');
            }
            targetTab = document.querySelector('.' + this.dataset.tab);
            targetTab.classList.add('mtabs__one--active');
        })
    }
}
if (quests){
    for (i=0; i<quests.length; i++){
        quests[i].addEventListener('click', function(){
            this.parentElement.parentElement.classList.toggle('question--open');
        })
    }
}
if (moreRev){
    moreRev.addEventListener('click', function(){
        this.parentElement.classList.add('reviews--open');
    })
}
if (moreProjRev){
    moreProjRev.addEventListener('click', function(){
        this.parentElement.parentElement.classList.add('reviews--open');
    })
}

if (showForm){
    showForm.addEventListener('click', function(){
        this.parentElement.classList.add('msale-form--show');
    })
}

let priceCat = document.getElementsByClassName('price-category__btn');
if (priceCat) {
    for (i = 0; i<priceCat.length; i++){
        priceCat[i].addEventListener('click',function(){
            this.parentElement.parentElement.classList.toggle('price-category--open');
        })
    }

}